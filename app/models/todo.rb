# == Schema Information
#
# Table name: todos
#
#  id         :integer         not null, primary key
#  todoitem   :string(255)
#  created_at :datetime        not null
#  updated_at :datetime        not null
#  user_id    :integer
#  complete   :boolean
#

class Todo < ActiveRecord::Base
  attr_accessible :todoitem, :complete, :failuretweeted
  belongs_to :user
  default_scope order: 'todos.created_at DESC'
  
  validates(:todoitem, presence: true, length: { maximum: 140 } )
  validates(:user_id, presence: true)
  
end
