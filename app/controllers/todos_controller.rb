class TodosController < ApplicationController
  before_filter :authenticate_user!, except: [:show] #should add :index later
  before_filter :correct_user,  only: :destroy
  
  def new
    @todo = Todo.new
  end
  
  def show
    @todo = Todo.find(params[:id])
  end
  
  def create
    @todo = current_user.todos.build(params[:todo])
    if @todo.save
      flash[:success] = "Good luck on your todo!"
      redirect_to root_path
    else 
      @feed_items = []
      render 'static_pages/home'
    end
  end
  
  def destroy
    @todo.destroy
    redirect_to root_path
  end
  
  def completed_toggle
    @todo = Todo.find(params[:id])
    if @todo.complete == false
      @todo.complete = true
    else
      @todo.complete = false
    end
    @todo.save
    redirect_to root_path
  end
  
  def failuretweeted_toggle
    @todo = Todo.find(params[:id])
    if @todo.failuretweeted == false
      @todo.failuretweeted = true
    else
      @todo.failuretweeted = false
    end
    @todo.save
  end
  
  private
    
    def correct_user
      @todo = current_user.todos.find_by_id(params[:id])
      redirect_to root_path if @todo.nil?
    end
  
end
