class TwitterController < ApplicationController

  def prepare_access_token(oauth_token, oauth_token_secret)
    consumer = OAuth::Consumer.new("feKovHisDFI6UD8quONVg", "2Kp9nblbSyHNKC7pENhK2DQ7espIIu5W2w1gWyabw", { :site => "http://api.twitter.com" })
    # now create the access token object from passed values
    token_hash = { :oauth_token => oauth_token,
                   :oauth_token_secret => oauth_token_secret
                 }
    access_token = OAuth::AccessToken.from_hash(consumer, token_hash)
    return access_token
  end
    
  def commit_tweet
    @todo = Todo.find(params[:id])
    auth = current_user.authentications.find(:first, :conditions => { :provider => 'twitter' })
    access_token = prepare_access_token(auth['token'], auth['secret'])
    commit_date = l @todo.created_at, :format => :shorter
    tweet = "I commit to " + @todo.todoitem + " on " + commit_date
    
    response = access_token.request(:post, "http://api.twitter.com/1/statuses/update.json",
                                    :status => tweet)

    flash[:success] = "Tweeted successfully."
    redirect_to root_path
  end
  
  def check_if_all_complete
    @user = User.find(params[:userid])
    time_allowed = 24.hours

    @user.todos.each do |todo|
      if todo.failuretweeted == false
        if Time.now - todo.created_at > time_allowed
          #todo was created less than 24 hours ago
          if todo.complete?
            #todo was completed, do nothing
          else
            failure_tweet(todo.id)
            failuretweeted_toggle(todo.id)
          end
        else
          #there were no todos created less than 24 hours ago, do nothing
        end
      else
        #failuretweet is false
      end
    end
  end

  def failure_tweet(todoid)
    auth = current_user.authentications.find(:first, :conditions => { :provider => 'twitter' })
    @todo = Todo.find(todoid)
    access_token = prepare_access_token(auth['token'], auth['secret'])
    failuretweet = "I did not complete " + @todo.todoitem 

    response = access_token.request(:post, "http://api.twitter.com/1/statuses/update.json",
                                    :status => failuretweet)
    failuretweeted_toggle(@todo.id)
  end
  
  def failuretweeted_toggle(todoid)
    @todo = Todo.find(todoid)
    @todo.failuretweeted = true
    @todo.save
  end
end
