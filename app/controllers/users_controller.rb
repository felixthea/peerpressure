class UsersController < ApplicationController
  
  def show
    @user = User.find(params[:id])
    @todos = @user.todos.paginate(page: params[:page])
  end
end