class CreateTodos < ActiveRecord::Migration
  def change
    create_table :todos do |t|
      t.string :todoitem

      t.timestamps
    end
  end
end
