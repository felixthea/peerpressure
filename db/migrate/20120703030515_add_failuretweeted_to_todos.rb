class AddFailuretweetedToTodos < ActiveRecord::Migration
  def change
    add_column :todos, :failuretweeted, :boolean, :default => false
  end
end
