class AddUserIDtoToDoTable < ActiveRecord::Migration
  def self.up
    add_column :todos, :user_id, :integer
    add_index :todos, [:user_id, :created_at]
  end
end
