require 'spec_helper'

describe "To do pages" do
  
  subject { page }
  
  describe "add todos page" do
    before { visit add_path }
    
    it { should have_selector('h1',     text: "Add todo") }
    it { should have_selector('title',  text: full_title('Add todo')) }
    
  end
  
  describe "todo page" do
    let(:todo) { FactoryGirl.create(:todo) }
    before { visit todo_path(todo) }
    
    it { should have_content(todo.todoitem) }
  end
  
  describe "adding a todo" do
    
    before { visit add_path }
    
    let(:submit) { "Commit Todo" }
    
    describe "with invalid information" do
      it "should not create a todo" do
        expect { click_button submit }.not_to change(Todo, :count)
      end
    end
    
    describe "with valid information" do
      before do
        fill_in "Your Todo",      with: "Go to the bank"
      end
      
      it "should create a todo" do
        expect { click_button submit}.to change(Todo, :count).by(1)
      end
    end
  end
end
