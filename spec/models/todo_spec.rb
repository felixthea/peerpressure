# == Schema Information
#
# Table name: todos
#
#  id         :integer         not null, primary key
#  todoitem   :string(255)
#  created_at :datetime        not null
#  updated_at :datetime        not null
#  user_id    :integer
#  complete   :boolean
#

require 'spec_helper'

describe Todo do
  
  before do
    @todo = Todo.new(todoitem: "Example Todo")
  end
  
  subject { @todo }
  
  it { should respond_to(:todoitem) }
  it { should be_valid }
  
  describe "when todoitem is not present" do
    before { @todo.todoitem = " " }
    it { should_not be_valid }
  end
  
  describe "when todoitem is too long" do
    before { @todo.todoitem = "a" * 1000 }
    it { should_not be_valid}
  end
  
end
