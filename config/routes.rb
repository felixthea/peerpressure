Peerpressure::Application.routes.draw do

  resources :authentications
  devise_for :users, :controllers => { :registrations => 'registrations' }
  resources :users, :only => [:show]
  resources :todos, :only => [:create, :destroy]
  
  root to: 'static_pages#home'
  
  match '/help',                                  to: 'static_pages#help'
  match '/add',                                   to: 'todos#new'
  match '/auth/:provider/callback',               to: 'authentications#create'
  match '/todos/:id/completed',                   to: 'todos#completed_toggle', :as => :toggle
  match '/twitter/:id/commit_tweet',              to: 'twitter#commit_tweet', :as => :commit_tweet
  match '/twitter/:userid/check_if_all_complete', to: 'twitter#check_if_all_complete', :as => :check_if_all_complete
 
end
